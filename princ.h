// CC-BY Edouard.Thiel@univ-amu.fr - 22/01/2019

#ifndef PRINC_H
#define PRINC_H

#include "ui_princ.h"

#include "paramdialog.h"

class Princ : public QMainWindow, private Ui::Princ
{
    Q_OBJECT

public:
    explicit Princ(QWidget *parent = 0);
private:
    ParamDialog* paramDialog;

public slots:
    void setSliderRadius(double radius);

protected slots:
    void onSliderRadius(int value);
    void paramAction();
};

#endif // PRINC_H
