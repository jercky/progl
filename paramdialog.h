#ifndef PARAMDIALOG_H
#define PARAMDIALOG_H

#include "ui_paramDialog.h"
#include "glarea.h"

class ParamDialog : public QDialog, private Ui::ParamDialog
{
    Q_OBJECT

public:
    explicit ParamDialog(QWidget *parent = nullptr, GLArea* _glarea = nullptr);
private:
    GLArea* glarea;
private slots:
    void on__btn_rotate_clicked();
    void on__btn_start_clicked();
    void on__btn_stop_clicked();
    void on__btn_sp_up_clicked();
    void on__btn_sp_down_clicked();
};

#endif // PARAMDIALOG_H
