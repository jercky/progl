#-------------------------------------------------
#
# Project created by QtCreator 2019-01-12T12:27:11
#
#-------------------------------------------------

QT       += core gui

CONFIG += c++14

LIBS += -lGLU

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ProjetPG
TEMPLATE = app


SOURCES += main.cpp\
        princ.cpp \
        glarea.cpp \
    cylindre.cpp \
    paramdialog.cpp \
    demicylindre.cpp

HEADERS  += princ.h \
        glarea.h \
    cylindre.h \
    paramdialog.h \
    demicylindre.h \
    forme.h

FORMS    += princ.ui \
    paramDialog.ui

RESOURCES += ProjetPG.qrc
