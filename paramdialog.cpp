#include "paramdialog.h"

#include <QDebug>

ParamDialog::ParamDialog(QWidget *parent, GLArea* _glarea) :
    QDialog(parent)
{
    setupUi(this);
    glarea = _glarea;

    connect (btn_rotate, SIGNAL(clicked()), this, SLOT(on__btn_rotate_clicked()));
    connect (btn_start, SIGNAL(clicked()), this, SLOT(on__btn_start_clicked()));
    connect (btn_stop, SIGNAL(clicked()), this, SLOT(on__btn_stop_clicked()));
    connect (btn_sp_up, SIGNAL(clicked()), this, SLOT(on__btn_sp_up_clicked()));
    connect (btn_sp_down, SIGNAL(clicked()), this, SLOT(on__btn_sp_down_clicked()));
}

void ParamDialog::on__btn_rotate_clicked()
{
    glarea->keyRotateEvent();
}

void ParamDialog::on__btn_start_clicked()
{
    glarea->keyStartEvent();
}

void ParamDialog::on__btn_stop_clicked()
{
    glarea->keyStopEvent();
}

void ParamDialog::on__btn_sp_up_clicked()
{
    glarea->keySpUpEvent();
}

void ParamDialog::on__btn_sp_down_clicked()
{
    glarea->keySpDownEvent();
}
