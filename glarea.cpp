// CC-BY Edouard.Thiel@univ-amu.fr - 22/01/2019

#include "glarea.h"
#include <GL/glu.h>
#include <QDebug>
#include <QSurfaceFormat>
#include <QMatrix4x4>

static const QString vertexShaderFile   = ":/vertex.glsl";
static const QString fragmentShaderFile = ":/fragment.glsl";


GLArea::GLArea(QWidget *parent) :
    QOpenGLWidget(parent)
{
    qDebug() << "init GLArea" ;

    QSurfaceFormat sf;
    sf.setDepthBufferSize(24);
    sf.setSamples(16);  // nb de sample par pixels : suréchantillonnage por l'antialiasing, en décalant à chaque fois le sommet
                        // cf https://www.khronos.org/opengl/wiki/Multisampling et https://stackoverflow.com/a/14474260
    setFormat(sf);
    qDebug() << "Depth is"<< format().depthBufferSize();

    setEnabled(true);  // événements clavier et souris
    setFocusPolicy(Qt::StrongFocus); // accepte focus
    setFocus();                      // donne le focus

    m_timer = new QTimer(this);
    m_timer->setInterval(50);  // msec
    connect (m_timer, SIGNAL(timeout()), this, SLOT(onTimeout()));
    connect (this, SIGNAL(radiusChanged(double)), this, SLOT(setRadius(double)));

}

void GLArea::tearGLObjects()
{
    delete[] cylindres;
    m_vbo.destroy();
}

GLArea::~GLArea()
{
    qDebug() << "destroy GLArea";

    tearGLObjects();
    delete m_timer;
    delete explosion;
    // Contrairement aux méthodes virtuelles initializeGL, resizeGL et repaintGL,
    // dans le destructeur le contexte GL n'est pas automatiquement rendu courant.
    makeCurrent();

    // ici destructions de ressources GL

    doneCurrent();
}

void GLArea::initializeGL()
{
    qDebug() << __FUNCTION__ ;
    initializeOpenGLFunctions();
    glEnable(GL_DEPTH_TEST);

    makeGLObjects();

    // Pour la gestion des explosions
    explosion = new short[2];
    explosion[0] = explosion[1] = 4;

    // shaders
    m_program = new QOpenGLShaderProgram(this);
    m_program->addShaderFromSourceFile(QOpenGLShader::Vertex, vertexShaderFile);  // compile
    m_program->addShaderFromSourceFile(QOpenGLShader::Fragment, fragmentShaderFile);
    if (! m_program->link()) {  // édition de lien des shaders dans le shader program
        qWarning("Failed to compile and link shader program:");
        qWarning() << m_program->log();
    }

    // récupère identifiants de "variables" dans les shaders
    m_posAttr = m_program->attributeLocation("posAttr");
    m_colAttr = m_program->attributeLocation("colAttr");
    m_matrixUniform = m_program->uniformLocation("mvMatrix");
}


void GLArea::makeGLObjects()
{
    // Construction des formes
    uint nb_cyl = 6;
    cylindres = new Forme*[nb_cyl];
    cylindres[0] = new Cylindre(0.8f,0.4f, 20,{1.0f,1.0f,1.0f});
    cylindres[1] = new Cylindre(1.6f,0.1f, 20,{0.0f,1.0f,0.0f});
    cylindres[2] = new Cylindre(0.25f,0.15f,20,{0.0f,0.0f,1.0f});
    cylindres[3] = new Cylindre(0.85f,0.175f,4,{0.2f,0.2f,0.2f});
    cylindres[4] = new DemiCylindre(0.4f,2.2f,0.6f,20,{0.1f,0.1f,0.1f});
    cylindres[5] = new DemiCylindre(0.4f,2.2f,0.6f,20,{0.9f,0.2f,0.2f});

    QVector<GLfloat> vert_data;
    for (uint i = 0 ; i < nb_cyl ; i++)
    {
        cylindres[i]->construire_forme();
        GLfloat* to_append_v = cylindres[i]->getVertices();
        GLfloat* to_append_c = cylindres[i]->getColors();
        for (uint j = 0 ; j < cylindres[i]->getIndex() ; j+=3)
        {
            // Vertices
            vert_data.append(to_append_v[j]);
            vert_data.append(to_append_v[j+1]);
            vert_data.append(to_append_v[j+2]);
            // Couleurs
            vert_data.append(to_append_c[j]);
            vert_data.append(to_append_c[j+1]);
            vert_data.append(to_append_c[j+2]);
        }
    }

    //VBO
    m_vbo.create();
    m_vbo.bind();
    m_vbo.allocate(vert_data.constData(), vert_data.count()*sizeof(GLfloat));
}

void GLArea::resizeGL(int w, int h)
{
    qDebug() << __FUNCTION__ << w << h;

    // C'est fait par défaut
    glViewport(0, 0, w, h);

    m_ratio = (double) w / h;
    // doProjection();
}

void GLArea::dessiner_cylindre(Forme* c, QMatrix4x4 matrix, int index)
{

    qDebug() << index << c->getIndex();
    m_program->setUniformValue(m_matrixUniform, matrix);
    glDrawArrays(GL_TRIANGLES, index, c->getIndex()/3);
}

int GLArea::getIndex(uint i)
{
    int r = 0;
    for (uint j = 0 ; j < i ; j++)
        r+=cylindres[j]->getIndex()/3;
    return r;
}

void GLArea::paintGL()
{
    qDebug() << __FUNCTION__ ;
//    glClearColor(0.8,0.2,0.2,0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    m_program->bind(); // active le shader program
    QMatrix4x4 matrix;
    GLfloat hr = m_radius, wr = hr * m_ratio;            // = glFrustum
    matrix.frustum(-wr, wr, -hr, hr, 1.0, 10.0);

    matrix.translate(-2, 0, -6.0);
    matrix.rotate(m_angle, 1, 0, 0);

    // Rotation de la scène pour l'animation
    m_program->setUniformValue(m_matrixUniform, matrix);

    m_program->setAttributeBuffer(m_posAttr,
            GL_FLOAT, 0 * sizeof(GLfloat), 3, 6 * sizeof(GLfloat));
    m_program->setAttributeBuffer(m_colAttr,
            GL_FLOAT, 3 * sizeof(GLfloat), 3, 6 * sizeof(GLfloat));

    m_program->enableAttributeArray(m_posAttr);
    m_program->enableAttributeArray(m_colAttr);


    QMatrix4x4 d_matrix = matrix;
    bool flag_exp = false;
    for (uint i = 0 ; i < 4 ; i++)
    {
        matrix = d_matrix;
        matrix.translate(1.2f*i,0,0);
        QMatrix4x4 matrix2 = matrix;

        m_anim = ((int)m_anim + 180*(i%2))%360;
        float p_2 = (m_anim*M_PI/180);

        float x_b = 0.65f*cos(p_2)+0.2f,
              y_v = cos(-p_2+M_PI/2)*0.5f,
              x_v = -1.625f+0.55f*sin(-p_2+M_PI/2);
        float beta = atan((y_v)/abs(x_b-x_v));
        float m_beta = beta/M_PI*180;

        // Cylindre
        matrix.rotate(-90,1,0,0);
        matrix.translate(0,0,0.2f);
        if (m_anim < speed && !flag_exp && explosion[0] != i && explosion[1] != i)
        {
            explosion[1] = explosion[0];
            explosion[0] = i;
            flag_exp = true;
        }
        if (i == explosion[0])
            dessiner_cylindre(cylindres[5],matrix,getIndex(5));
        else
            dessiner_cylindre(cylindres[4],matrix,getIndex(4));
        matrix = matrix2;
        // ---
        // Vilebrequin perpendiculaires [Liens]
        matrix.translate(-0.5f, -1.6f,0);
        matrix.rotate(m_anim, 1,0,0);
        matrix.rotate(90,0,1,0);
        dessiner_cylindre(cylindres[2],matrix,getIndex(2));
        matrix.translate(0, 0,1.0f);
        dessiner_cylindre(cylindres[2],matrix,getIndex(2));
        // ---
        matrix  = matrix2;
        // Vilebrequin parallèles [Rectangles]
        matrix.translate(-0.25f, -1.65f,0);
        matrix.rotate(m_anim, 1,0,0);
        matrix.translate(0, 0.25f,0);
        matrix.rotate(90,1,0,0);
        matrix.rotate(45,0,0,1);
        dessiner_cylindre(cylindres[3],matrix,getIndex(3));
        matrix.rotate(-45,0,0,1);
        matrix.translate(0.5f, 0,0);
        matrix.rotate(45,0,0,1);
        dessiner_cylindre(cylindres[3],matrix,getIndex(3));
        // ---
        // Vilebrequin lié au piston [Central]
        matrix = matrix2;
        matrix.translate(0,x_v,y_v);
        matrix.rotate(180,1,0,1);
        dessiner_cylindre(cylindres[2],matrix,getIndex(2));
        // ---
        // Piston
        matrix = matrix2;
        matrix.translate(0,0.65f*cos(p_2)+0.2f,0);
        matrix.rotate(90,1,0,0);
        dessiner_cylindre(cylindres[0],matrix,getIndex(0));
        // Bielle
        matrix.translate(0,sin(beta),cos(beta));
        matrix.rotate(-m_beta,1,0,0);
        dessiner_cylindre(cylindres[1],matrix,getIndex(1));
    }

    m_program->disableAttributeArray(m_posAttr);
    m_program->disableAttributeArray(m_colAttr);
    m_program->release();
}

void GLArea::keyRotateEvent()
{
    m_angle += 1;
    if (m_angle >= 360) m_angle -= 360;
    update();
}
void GLArea::keyStartEvent()
{
    m_timer->start();
}
void GLArea::keyStopEvent()
{
    m_timer->stop();
}
void GLArea::keySpUpEvent()
{
    setSpeed(speed+0.5);
}
void GLArea::keySpDownEvent()
{
    setSpeed(speed-0.5);
}

void GLArea::keyPressEvent(QKeyEvent *ev)
{
    qDebug() << __FUNCTION__ << ev->text();

    switch(ev->key()) {
        case Qt::Key_Space :
            keyRotateEvent();
            break;
        case Qt::Key_D :
            if (!m_timer->isActive())
                keyStartEvent();
            break;
        case Qt::Key_S :
            if (m_timer->isActive())
                keyStopEvent();
            break;
        case Qt::Key_A :
            if (ev->text() == "a")
                 keySpDownEvent();
            else keySpUpEvent();
            break;
        case Qt::Key_R :
            if (ev->text() == "r")
                 setRadius(m_radius-0.05);
            else setRadius(m_radius+0.05);
            break;
    }
}

void GLArea::keyReleaseEvent(QKeyEvent *ev)
{
    qDebug() << __FUNCTION__ << ev->text();
}

void GLArea::mousePressEvent(QMouseEvent *ev)
{
    qDebug() << __FUNCTION__ << ev->x() << ev->y() << ev->button();
}

void GLArea::mouseReleaseEvent(QMouseEvent *ev)
{
    qDebug() << __FUNCTION__ << ev->x() << ev->y() << ev->button();
}

void GLArea::mouseMoveEvent(QMouseEvent *ev)
{
    qDebug() << __FUNCTION__ << ev->x() << ev->y();
}

void GLArea::onTimeout()
{
    qDebug() << __FUNCTION__ ;
    m_anim += speed;
    if (m_anim >= 360) m_anim -= 360;
    update();
}

void GLArea::setRadius(double radius)
{
    qDebug() << __FUNCTION__ << radius << sender();
    if (radius != m_radius && radius > 0.01 && radius <= 10) {
        m_radius = radius;
        qDebug() << "  emit radiusChanged()";
        emit radiusChanged(radius);
        update();
    }
}

void GLArea::setSpeed(double newSpeed)
{
    qDebug() << __FUNCTION__ << newSpeed;
    if (newSpeed != speed && newSpeed > 0.5 && newSpeed <= 20) {
        speed = newSpeed;
        update();
    }
}




