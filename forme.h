#ifndef FORME_H
#define FORME_H

#include <GL/glu.h>
#include <math.h>
#include <QOpenGLWidget>
#include <QOpenGLFunctions>
#include <QOpenGLShaderProgram>

class Forme
{
public:
    Forme() {}
    virtual ~Forme(){}
    virtual void construire_forme() {}
    virtual GLfloat* getVertices() const {}
    virtual GLfloat* getColors() const {}
    virtual unsigned getIndex() const {}
};

#endif // FORME_H
