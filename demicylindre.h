#ifndef DEMICYLINDRE_H
#define DEMICYLINDRE_H

#include "cylindre.h"

class DemiCylindre: public Cylindre
{

private:
    float rayon;
protected:
    void construire_cercle_d(int* p_index, int i, float ep);
    void generer_matrice_d();
    void generer_couleur_d();
public:
    DemiCylindre(float _rayon = 0.2f,
                 float _ep_cyl = 0.5f,
                 float _r_cyl = 0.8f,
                 unsigned _nb_fac = 20,
                 Color _color = {1.0f,0.0f,0.0f});
    ~DemiCylindre();
    void construire_demiCylindre();

    void construire_forme() override{construire_demiCylindre();}
};

#endif // DEMICYLINDRE_H
