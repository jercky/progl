#include "demicylindre.h"

DemiCylindre::DemiCylindre(float _rayon,float _ep_cyl,float _r_cyl,unsigned _nb_fac,Color _color)
    :rayon(_rayon),Cylindre(_ep_cyl,_r_cyl,_nb_fac,_color)
{
    setIndex(18*uint(_nb_fac/2)*3);
}
DemiCylindre::~DemiCylindre()
{
}

void DemiCylindre::generer_matrice_d()
{
    int t_index = 0;

    for (unsigned i = 0 ; i < uint(getNbFac()/2) ; i++)
    {
        construire_cercle_d(&t_index, i, -getEpCyl()/2.0f);
        construire_cercle_d(&t_index, i, getEpCyl()/2.0f);
        construire_facettes(&t_index, i);
    }
}

void DemiCylindre::construire_demiCylindre()
{
    generer_matrice_d();
    generer_couleur_d();
}

void DemiCylindre::generer_couleur_d()
{
    for (uint i = 0 ; i < getIndex() ; i+=18)
    {
        for (uint j = 0 ; j < 36 ; j+=3)
        {
            *colour(i+j)   = getColor().r;
            *colour(i+j+1) = getColor().g;
            *colour(i+j+2) = getColor().b;
        }
        i+=36;

        for (uint j = 0 ; j < 18 ; j+=3)
        {
            *colour(i+j)   = getColor().r*0.8f;
            *colour(i+j+1) = getColor().g*0.8f;
            *colour(i+j+2) = getColor().b*0.8f;
        }
    }
}

void DemiCylindre::construire_cercle_d(int *p_index, int i, float ep)
{
    uint index = *p_index;
    float rangle = 2*M_PI/getNbFac();

    *vertex(index)     = rayon*cos(rangle*i);
    *vertex(index+1)   = rayon*sin(rangle*i);
    *vertex(index+2)   = ep;
    index += 3;

    *vertex(index)     = getRCyl()*cos(rangle*i);
    *vertex(index+1)   = getRCyl()*sin(rangle*i);
    *vertex(index+2)   = ep;
    index+=3;

    *vertex(index)     = getRCyl()*cos(rangle*(i+1));
    *vertex(index+1)   = getRCyl()*sin(rangle*(i+1));
    *vertex(index+2)   = ep;
    index+=3;

    *vertex(index)     = getRCyl()*cos(rangle*(i+1));
    *vertex(index+1)   = getRCyl()*sin(rangle*(i+1));
    *vertex(index+2)   = ep;
    index+=3;

    *vertex(index)     = rayon*cos(rangle*i);
    *vertex(index+1)   = rayon*sin(rangle*i);
    *vertex(index+2)   = ep;
    index+=3;

    *vertex(index)     = rayon*cos(rangle*(i+1));
    *vertex(index+1)   = rayon*sin(rangle*(i+1));
    *vertex(index+2)   = ep;
    index+=3;

    *p_index = index;
}

/*
    *vertex(index)     = rayon*cos(rangle*i);
    *vertex(index+1)   = rayon*sin(rangle*i);
    *vertex(index+2)   = ep;
    index += 3;

    *vertex(index)     = getRCyl()*cos(rangle*i);
    *vertex(index+1)   = getRCyl()*sin(rangle*i);
    *vertex(index+2)   = ep;
    index+=3;

    *vertex(index)     = getRCyl()*cos(rangle*(i+1));
    *vertex(index+1)   = getRCyl()*sin(rangle*(i+1));
    *vertex(index+2)   = ep;
    index+=3;

    *vertex(index)     = getRCyl()*cos(rangle*(i+1));
    *vertex(index+1)   = getRCyl()*sin(rangle*(i+1));
    *vertex(index+2)   = ep;
    index+=3;

    *vertex(index)     = getRCyl()*cos(rangle*i);
    *vertex(index+1)   = getRCyl()*sin(rangle*i);
    *vertex(index+2)   = ep;
    index+=3;

    *vertex(index)     = rayon*cos(rangle*(i+1));
    *vertex(index+1)   = rayon*sin(rangle*(i+1));
    *vertex(index+2)   = ep;
    index+=3;
*/
