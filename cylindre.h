#ifndef CYLINDRE_H
#define CYLINDRE_H

#include "forme.h"

#define M_PI       3.14159265358979323846f

typedef struct {
    float r;
    float g;
    float b;
} Color;

class Cylindre : public Forme
{
private:
    float ep_cyl;
    float r_cyl;
    unsigned nb_fac;
    Color color;

    GLfloat* vertices;
    GLfloat* colors;
    uint index;

protected:
    void dessiner_cercle();
    void construire_cercle(int* p_index, int i, float ep);
    void dessiner_facettes();
    void construire_facettes(int* p_index, int i);

    void generer_matrice();
    void generer_couleur();

    void setIndex(const uint index);

    inline GLfloat* vertex(uint i) {return &vertices[i];}
    inline GLfloat* colour(uint i) {return &colors[i];}

public:
    Cylindre(float _ep_cyl = 0.5f,
             float _r_cyl = 0.8f,
             unsigned _nb_fac = 20,
             Color _color = {1.0f,0.0f,0.0f});
    ~Cylindre();

    void dessiner_cylindre();
    void construire_cylindre();

    void construire_forme() override {construire_cylindre();}

    inline uint getIndex() const {return index;}
    inline GLfloat* getVertices() const {return vertices;}
    inline GLfloat* getColors() const {return colors;}

    inline float getEpCyl() const {return ep_cyl;}
    inline float getRCyl() const {return r_cyl;}
    inline uint getNbFac() const {return nb_fac;}
    inline Color getColor() const {return color;}
};

#endif // CYLINDRE_H
