#include "cylindre.h"

Cylindre::Cylindre(float _ep_cyl,
                   float _r_cyl,
                   unsigned _nb_fac,
                   Color _color)
: ep_cyl(_ep_cyl), r_cyl(_r_cyl), nb_fac(_nb_fac), color(_color), Forme()
{
    index = 12*nb_fac*3;
    vertices = new GLfloat[index];
    colors = new GLfloat[index];
}

Cylindre::~Cylindre()
{
    delete vertices;
    delete colors;
}

void Cylindre::setIndex(const uint _index)
{
    delete vertices;
    delete colors;

    index = _index;

    vertices = new GLfloat[index];
    colors = new GLfloat[index];
}

void Cylindre::dessiner_cercle()
{
    float rangle = 2*M_PI/nb_fac;
    glBegin(GL_POLYGON);;
    glVertex3f(0,0,0);
    glVertex3f(r_cyl,0,0);
    glVertex3f(r_cyl*cos(rangle), r_cyl*sin(rangle),0);
    glVertex3f(r_cyl*cos(rangle/2), r_cyl*sin(rangle/2),0);
    glVertex3f(r_cyl,0,0);
    glEnd();
}

void Cylindre::construire_cercle(int* p_index, int i, float ep)
{
    int index = *p_index;
    float rangle = 2*M_PI/nb_fac;

    vertices[index]     = 0;
    vertices[index+1]   = 0;
    vertices[index+2]   = ep;
    index += 3;

    vertices[index]     = r_cyl*cos(rangle*i);
    vertices[index+1]   = r_cyl*sin(rangle*i);
    vertices[index+2]   = ep;
    index+=3;

    vertices[index]     = r_cyl*cos(rangle*(i+1));
    vertices[index+1]   = r_cyl*sin(rangle*(i+1));
    vertices[index+2]   = ep;
    index+=3;

    *p_index = index;
}

void Cylindre::dessiner_facettes()
{
    float rangle = 2*M_PI/nb_fac;
    glBegin(GL_QUADS);

    glVertex3f(r_cyl,0,0);
    glVertex3f(r_cyl*cos(rangle), r_cyl*sin(rangle),0);
    glVertex3f(r_cyl*cos(rangle), r_cyl*sin(rangle),ep_cyl);
    glVertex3f(r_cyl,0,ep_cyl);

    glVertex3f(r_cyl,0,0);
    glVertex3f(r_cyl*cos(rangle/2), r_cyl*sin(rangle/2),0);
    glVertex3f(r_cyl*cos(rangle/2), r_cyl*sin(rangle/2),ep_cyl);
    glVertex3f(r_cyl,0,ep_cyl);

    glEnd();
}

void Cylindre::construire_facettes(int* p_index, int i)
{
    int index = *p_index;
    float rangle = 2*M_PI/nb_fac;

    vertices[index]     = r_cyl*cos(rangle*i);
    vertices[index+1]   = r_cyl*sin(rangle*i);
    vertices[index+2] = -ep_cyl/2;
    index += 3;

    vertices[index]   = r_cyl*cos(rangle*(i+1));
    vertices[index+1] = r_cyl*sin(rangle*(i+1));
    vertices[index+2] = -ep_cyl/2;
    index += 3;

    vertices[index]   = r_cyl*cos(rangle*(i+1));
    vertices[index+1] = r_cyl*sin(rangle*(i+1));
    vertices[index+2] = ep_cyl/2;
    index += 3;

    vertices[index]   = r_cyl*cos(rangle*(i+1));
    vertices[index+1] = r_cyl*sin(rangle*(i+1));
    vertices[index+2] = ep_cyl/2;
    index += 3;

    vertices[index]     = r_cyl*cos(rangle*i);
    vertices[index+1]   = r_cyl*sin(rangle*i);
    vertices[index+2] = ep_cyl/2;
    index += 3;

    vertices[index]     = r_cyl*cos(rangle*i);
    vertices[index+1]   = r_cyl*sin(rangle*i);
    vertices[index+2] = -ep_cyl/2;
    index += 3;

    *p_index = index;
}

void Cylindre::dessiner_cylindre()
{
    for (unsigned i = 0 ; i < nb_fac ; i++)
    {
        glColor3f(color.r, color.g, color.b);
        float angle = 360*i/nb_fac;

        glPushMatrix();

        glRotatef(angle, 0,0,1);
        glTranslatef(0,0,ep_cyl/2);

        dessiner_cercle();

        glTranslatef(0,0,-ep_cyl);
        dessiner_cercle();

        glColor3f(color.r*0.8f, color.g*0.8f, color.b*0.8f);
        dessiner_facettes();

        glPopMatrix();
    }
}

void Cylindre::construire_cylindre()
{
    generer_matrice();
    generer_couleur();
}

void Cylindre::generer_matrice()
{
    int t_index = 0;

    for (unsigned i = 0 ; i < nb_fac ; i++)
    {
        construire_cercle(&t_index, i, -ep_cyl/2.0f);
        construire_cercle(&t_index, i, ep_cyl/2.0f);
        construire_facettes(&t_index, i);
    }

    if (t_index != index) qDebug() << "Index différent à la construction !";
}

void Cylindre::generer_couleur()
{
    for (uint i = 0 ; i < index ; i+=18)
    {
        for (uint j = 0 ; j < 18 ; j+=3)
        {
            colors[i+j]   = color.r;
            colors[i+j+1] = color.g;
            colors[i+j+2] = color.b;
        }
        i+=18;

        for (uint j = 0 ; j < 18 ; j+=3)
        {
            colors[i+j]   = color.r*0.8f;
            colors[i+j+1] = color.g*0.8f;
            colors[i+j+2] = color.b*0.8f;
        }
    }
}
